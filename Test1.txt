*** Settings ***
Library  Selenium2Library
Test Teardown  Close All Browsers

*** Variables ***
${HOMEPAGE}  https://www.google.com.ua
${BROWSER}  gc

*** Test Cases ***
Open Google
  Go to homepage
  Google and check results  SoftBistro  https://softbistro.com	
	
*** Keywords ***
Go to homepage
  Open Browser  ${HOMEPAGE}  ${BROWSER}
Google and check results
  [Arguments]  ${searchkey}  ${result}
  Input Text  id=lst-ib  ${searchkey}
  Click Button  id=_fZl
  Wait Until Page Contains  ${result}

